Endpoint: http://api.alquran.cloud/v1/edition [GET] 
- Scenario 01: Verify if able to filter by format (Eg: "audio", "text")
- Parameter: format [Key] text [Value]
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.muyassar",
            "language": "ar",
            "name": "تفسير المیسر",
            "englishName": "King Fahad Quran Complex",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        },
        {
            "identifier": "az.mammadaliyev",
            "language": "az",
            "name": "Məmmədəliyev & Bünyadov",
            "englishName": "Vasim Mammadaliyev and Ziya Bunyadov",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        }
    ]
}
```
- Parameter: format [Key] audio [Value]
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.abdulbasitmurattal",
            "language": "ar",
            "name": "عبد الباسط عبد الصمد المرتل",
            "englishName": "Abdul Basit",
            "format": "audio",
            "type": "translation",
            "direction": null
        },
        {
            "identifier": "ar.abdullahbasfar",
            "language": "ar",
            "name": "عبد الله بصفر",
            "englishName": "Abdullah Basfar",
            "format": "audio",
            "type": "versebyverse",
            "direction": null
        }
    ]
}
```

- Scenario 02: Verify if invalid format entered
- Parameter: format [Key] image [Value]
- Result: Passed
```
{
    "code": 404,
    "status": "NOT FOUND",
    "data": "Invalid format."
}
```

- Scenario 03: Verify if able to filter by englishName (Eg: "Alikhan Musayev")
- Parameter: englishName [Key] Alikhan Musayev [Value]
- Result: Failed, display unrelated list
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.muyassar",
            "language": "ar",
            "name": "تفسير المیسر",
            "englishName": "King Fahad Quran Complex",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        },
        {
            "identifier": "az.mammadaliyev",
            "language": "az",
            "name": "Məmmədəliyev & Bünyadov",
            "englishName": "Vasim Mammadaliyev and Ziya Bunyadov",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        },
        {
            "identifier": "az.musayev",
            "language": "az",
            "name": "Musayev",
            "englishName": "Alikhan Musayev",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        }
    ]
}
```
- Result: Expected
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "az.musayev",
            "language": "az",
            "name": "Musayev",
            "englishName": "Alikhan Musayev",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        }
    ]
}
```

- Scenario 04: Verify if able to filter by type (Eg: "translation", "tafsir")
- Parameter: type [Key] translation [Value]
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "az.mammadaliyev",
            "language": "az",
            "name": "Məmmədəliyev & Bünyadov",
            "englishName": "Vasim Mammadaliyev and Ziya Bunyadov",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        },
        {
            "identifier": "az.musayev",
            "language": "az",
            "name": "Musayev",
            "englishName": "Alikhan Musayev",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        }
    ]
}
```
- Parameter: type [Key] tafsir [Value]
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.muyassar",
            "language": "ar",
            "name": "تفسير المیسر",
            "englishName": "King Fahad Quran Complex",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        },
        {
            "identifier": "ar.jalalayn",
            "language": "ar",
            "name": "تفسير الجلالين",
            "englishName": "Jalal ad-Din al-Mahalli and Jalal ad-Din as-Suyuti",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        }
    ]
}
```

- Scenario 05: Verify if invalid type entered
- Parameter: type [Key] sing [Value]
- Result: Passed
```
{
    "code": 404,
    "status": "NOT FOUND",
    "data": "Invalid type."
}
```


- Scenario 06: Verify if able to multi-filter by format, englishName & language
- Parameter: format [Key] text [Value], englishName [Key] King Fahad Quran Complex [Value], type [Key] tafsir [Value]
- Result: Failed, display unrelated list
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.muyassar",
            "language": "ar",
            "name": "تفسير المیسر",
            "englishName": "King Fahad Quran Complex",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        },
        {
            "identifier": "ar.jalalayn",
            "language": "ar",
            "name": "تفسير الجلالين",
            "englishName": "Jalal ad-Din al-Mahalli and Jalal ad-Din as-Suyuti",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        }
    ]
}
```
- Result: Expected
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "identifier": "ar.muyassar",
            "language": "ar",
            "name": "تفسير المیسر",
            "englishName": "King Fahad Quran Complex",
            "format": "text",
            "type": "tafsir",
            "direction": "rtl"
        }
    ]
}
```
