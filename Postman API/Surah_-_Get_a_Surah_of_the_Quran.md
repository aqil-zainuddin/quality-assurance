Endpoint: http://api.alquran.cloud/v1/surah [GET]
- Scenario 01: Verify if able to display a list of Surahs in the Quran
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "number": 1,
            "name": "سُورَةُ ٱلْفَاتِحَةِ",
            "englishName": "Al-Faatiha",
            "englishNameTranslation": "The Opening",
            "numberOfAyahs": 7,
            "revelationType": "Meccan"
        },
        {
            "number": 2,
            "name": "سُورَةُ البَقَرَةِ",
            "englishName": "Al-Baqara",
            "englishNameTranslation": "The Cow",
            "numberOfAyahs": 286,
            "revelationType": "Medinan"
        },
        {
            "number": 3,
            "name": "سُورَةُ آلِ عِمۡرَانَ",
            "englishName": "Aal-i-Imraan",
            "englishNameTranslation": "The Family of Imraan",
            "numberOfAyahs": 200,
            "revelationType": "Medinan"
        },
        {
            "number": 4,
            "name": "سُورَةُ النِّسَاءِ",
            "englishName": "An-Nisaa",
            "englishNameTranslation": "The Women",
            "numberOfAyahs": 176,
            "revelationType": "Medinan"
        },
        {
            "number": 5,
            "name": "سُورَةُ المَائـِدَةِ",
            "englishName": "Al-Maaida",
            "englishNameTranslation": "The Table",
            "numberOfAyahs": 120,
            "revelationType": "Medinan"
        },
        {
            "number": 6,
            "name": "سُورَةُ الأَنۡعَامِ",
            "englishName": "Al-An'aam",
            "englishNameTranslation": "The Cattle",
            "numberOfAyahs": 165,
            "revelationType": "Meccan"
        },
        {
            "number": 7,
            "name": "سُورَةُ الأَعۡرَافِ",
            "englishName": "Al-A'raaf",
            "englishNameTranslation": "The Heights",
            "numberOfAyahs": 206,
            "revelationType": "Meccan"
        },
        {
            "number": 8,
            "name": "سُورَةُ الأَنفَالِ",
            "englishName": "Al-Anfaal",
            "englishNameTranslation": "The Spoils of War",
            "numberOfAyahs": 75,
            "revelationType": "Medinan"
        },
        {
            "number": 9,
            "name": "سُورَةُ التَّوۡبَةِ",
            "englishName": "At-Tawba",
            "englishNameTranslation": "The Repentance",
            "numberOfAyahs": 129,
            "revelationType": "Medinan"
        },
        {
            "number": 10,
            "name": "سُورَةُ يُونُسَ",
            "englishName": "Yunus",
            "englishNameTranslation": "Jonas",
            "numberOfAyahs": 109,
            "revelationType": "Meccan"
        },
        {
            "number": 11,
            "name": "سُورَةُ هُودٍ",
            "englishName": "Hud",
            "englishNameTranslation": "Hud",
            "numberOfAyahs": 123,
            "revelationType": "Meccan"
        },
        {
            "number": 12,
            "name": "سُورَةُ يُوسُفَ",
            "englishName": "Yusuf",
            "englishNameTranslation": "Joseph",
            "numberOfAyahs": 111,
            "revelationType": "Meccan"
        },
        {
            "number": 13,
            "name": "سُورَةُ الرَّعۡدِ",
            "englishName": "Ar-Ra'd",
            "englishNameTranslation": "The Thunder",
            "numberOfAyahs": 43,
            "revelationType": "Medinan"
        },
        {
            "number": 14,
            "name": "سُورَةُ إِبۡرَاهِيمَ",
            "englishName": "Ibrahim",
            "englishNameTranslation": "Abraham",
            "numberOfAyahs": 52,
            "revelationType": "Meccan"
        },
        {
            "number": 15,
            "name": "سُورَةُ الحِجۡرِ",
            "englishName": "Al-Hijr",
            "englishNameTranslation": "The Rock",
            "numberOfAyahs": 99,
            "revelationType": "Meccan"
        }
    ]
}
```

Endpoint: http://api.alquran.cloud/v1/surah/114/{{edition}} [GET]
- Scenario 01: Verify if able to display the requested Surahs from particular edition by filtering based on identifier
- http://api.alquran.cloud/v1/surah/114/en.asad
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": {
        "number": 114,
        "name": "سُورَةُ النَّاسِ",
        "englishName": "An-Naas",
        "englishNameTranslation": "Mankind",
        "revelationType": "Meccan",
        "numberOfAyahs": 6,
        "ayahs": [
            {
                "number": 6231,
                "text": "SAY: \"I seek refuge with the Sustainer of men,",
                "numberInSurah": 1,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6232,
                "text": "\"the Sovereign of men,",
                "numberInSurah": 2,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6233,
                "text": "\"the God of men,",
                "numberInSurah": 3,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6234,
                "text": "\"from the evil of the whispering, elusive tempter",
                "numberInSurah": 4,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6235,
                "text": "\"who whispers in the hearts of men",
                "numberInSurah": 5,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6236,
                "text": "\"from all [temptation to evil by] invisible forces as well as men,\"",
                "numberInSurah": 6,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            }
        ],
        "edition": {
            "identifier": "en.asad",
            "language": "en",
            "name": "Asad",
            "englishName": "Muhammad Asad",
            "format": "text",
            "type": "translation",
            "direction": "ltr"
        }
    }
}
```
- http://api.alquran.cloud/v1/surah/114/ar.alafasy
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": {
        "number": 114,
        "name": "سُورَةُ النَّاسِ",
        "englishName": "An-Naas",
        "englishNameTranslation": "Mankind",
        "revelationType": "Meccan",
        "numberOfAyahs": 6,
        "ayahs": [
            {
                "number": 6231,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6231.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6231.mp3"
                ],
                "text": "بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ قُلْ أَعُوذُ بِرَبِّ ٱلنَّاسِ",
                "numberInSurah": 1,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6232,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6232.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6232.mp3"
                ],
                "text": "مَلِكِ ٱلنَّاسِ",
                "numberInSurah": 2,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6233,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6233.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6233.mp3"
                ],
                "text": "إِلَٰهِ ٱلنَّاسِ",
                "numberInSurah": 3,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6234,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6234.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6234.mp3"
                ],
                "text": "مِن شَرِّ ٱلْوَسْوَاسِ ٱلْخَنَّاسِ",
                "numberInSurah": 4,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6235,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6235.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6235.mp3"
                ],
                "text": "ٱلَّذِى يُوَسْوِسُ فِى صُدُورِ ٱلنَّاسِ",
                "numberInSurah": 5,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6236,
                "audio": "https://cdn.islamic.network/quran/audio/128/ar.alafasy/6236.mp3",
                "audioSecondary": [
                    "https://cdn.islamic.network/quran/audio/64/ar.alafasy/6236.mp3"
                ],
                "text": "مِنَ ٱلْجِنَّةِ وَٱلنَّاسِ",
                "numberInSurah": 6,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            }
        ],
        "edition": {
            "identifier": "ar.alafasy",
            "language": "ar",
            "name": "مشاري العفاسي",
            "englishName": "Alafasy",
            "format": "audio",
            "type": "versebyverse",
            "direction": null
        }
    }
}
```


- Scenario 02: Verify if invalid identifier entered
- http://api.alquran.cloud/v1/surah/114/ar.umni
- Result: Failed, display the wrong edition
```
{
    "code": 200,
    "status": "OK",
    "data": {
        "number": 114,
        "name": "سُورَةُ النَّاسِ",
        "englishName": "An-Naas",
        "englishNameTranslation": "Mankind",
        "revelationType": "Meccan",
        "numberOfAyahs": 6,
        "ayahs": [
            {
                "number": 6231,
                "text": "بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ قُلْ أَعُوذُ بِرَبِّ النَّاسِ",
                "numberInSurah": 1,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6232,
                "text": "مَلِكِ النَّاسِ",
                "numberInSurah": 2,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6233,
                "text": "إِلَٰهِ النَّاسِ",
                "numberInSurah": 3,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6234,
                "text": "مِنْ شَرِّ الْوَسْوَاسِ الْخَنَّاسِ",
                "numberInSurah": 4,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6235,
                "text": "الَّذِي يُوَسْوِسُ فِي صُدُورِ النَّاسِ",
                "numberInSurah": 5,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            },
            {
                "number": 6236,
                "text": "مِنَ الْجِنَّةِ وَالنَّاسِ",
                "numberInSurah": 6,
                "juz": 30,
                "manzil": 7,
                "page": 604,
                "ruku": 556,
                "hizbQuarter": 240,
                "sajda": false
            }
        ],
        "edition": {
            "identifier": "quran-simple",
            "language": "ar",
            "name": "القرآن الكريم المبسط (تشكيل بسيط)",
            "englishName": "Simple",
            "format": "text",
            "type": "quran",
            "direction": "rtl"
        }
    }
}
```


Endpoint: http://api.alquran.cloud/v1/surah/114/editions/{{edition}},{{edition}},{{edition}} [GET]
- Scenario 01: Verify if able to display the requested Surahs from multiple edition by listed identifier
- http://api.alquran.cloud/v1/surah/114/editions/quran-uthmani,en.asad,en.pickthall
- Result: Passed
```
{
    "code": 200,
    "status": "OK",
    "data": [
        {
            "number": 114,
            "name": "سُورَةُ النَّاسِ",
            "englishName": "An-Naas",
            "englishNameTranslation": "Mankind",
            "revelationType": "Meccan",
            "numberOfAyahs": 6,
            "ayahs": [
                {
                    "number": 6231,
                    "text": "بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ قُلْ أَعُوذُ بِرَبِّ ٱلنَّاسِ",
                    "numberInSurah": 1,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6232,
                    "text": "مَلِكِ ٱلنَّاسِ",
                    "numberInSurah": 2,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6233,
                    "text": "إِلَٰهِ ٱلنَّاسِ",
                    "numberInSurah": 3,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6234,
                    "text": "مِن شَرِّ ٱلْوَسْوَاسِ ٱلْخَنَّاسِ",
                    "numberInSurah": 4,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6235,
                    "text": "ٱلَّذِى يُوَسْوِسُ فِى صُدُورِ ٱلنَّاسِ",
                    "numberInSurah": 5,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6236,
                    "text": "مِنَ ٱلْجِنَّةِ وَٱلنَّاسِ",
                    "numberInSurah": 6,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                }
            ],
            "edition": {
                "identifier": "quran-uthmani",
                "language": "ar",
                "name": "القرآن الكريم برسم العثماني",
                "englishName": "Uthmani",
                "format": "text",
                "type": "quran",
                "direction": "rtl"
            }
        },
        {
            "number": 114,
            "name": "سُورَةُ النَّاسِ",
            "englishName": "An-Naas",
            "englishNameTranslation": "Mankind",
            "revelationType": "Meccan",
            "numberOfAyahs": 6,
            "ayahs": [
                {
                    "number": 6231,
                    "text": "SAY: \"I seek refuge with the Sustainer of men,",
                    "numberInSurah": 1,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6232,
                    "text": "\"the Sovereign of men,",
                    "numberInSurah": 2,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6233,
                    "text": "\"the God of men,",
                    "numberInSurah": 3,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6234,
                    "text": "\"from the evil of the whispering, elusive tempter",
                    "numberInSurah": 4,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6235,
                    "text": "\"who whispers in the hearts of men",
                    "numberInSurah": 5,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6236,
                    "text": "\"from all [temptation to evil by] invisible forces as well as men,\"",
                    "numberInSurah": 6,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                }
            ],
            "edition": {
                "identifier": "en.asad",
                "language": "en",
                "name": "Asad",
                "englishName": "Muhammad Asad",
                "format": "text",
                "type": "translation",
                "direction": "ltr"
            }
        },
        {
            "number": 114,
            "name": "سُورَةُ النَّاسِ",
            "englishName": "An-Naas",
            "englishNameTranslation": "Mankind",
            "revelationType": "Meccan",
            "numberOfAyahs": 6,
            "ayahs": [
                {
                    "number": 6231,
                    "text": "Say: I seek refuge in the Lord of mankind,",
                    "numberInSurah": 1,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6232,
                    "text": "The King of mankind,",
                    "numberInSurah": 2,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6233,
                    "text": "The god of mankind,",
                    "numberInSurah": 3,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6234,
                    "text": "From the evil of the sneaking whisperer,",
                    "numberInSurah": 4,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6235,
                    "text": "Who whispereth in the hearts of mankind,",
                    "numberInSurah": 5,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                },
                {
                    "number": 6236,
                    "text": "Of the jinn and of mankind.",
                    "numberInSurah": 6,
                    "juz": 30,
                    "manzil": 7,
                    "page": 604,
                    "ruku": 556,
                    "hizbQuarter": 240,
                    "sajda": false
                }
            ],
            "edition": {
                "identifier": "en.pickthall",
                "language": "en",
                "name": "Pickthall",
                "englishName": "Mohammed Marmaduke William Pickthall",
                "format": "text",
                "type": "translation",
                "direction": "ltr"
            }
        }
    ]
}
```

