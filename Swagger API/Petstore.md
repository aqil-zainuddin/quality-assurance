Endpoint: https://petstore.swagger.io/#/pet/addPet [POST]
- Scenario 01: Verify able to add a new pet data with status ("available")
- Result: Passed
```
// Adding data (Before)
{
    "id": 9900,
    "category": {
        "id": 9900,
        "name": "cats"
    },
    "name": "momo",
    "photoUrls": [
        "string"
    ],
    "tags": [
        {
        "id": 9900,
        "name": "momo the cat"
        }
    ],
    "status": "available"
}
```
```
// Successful in storing the data (After)
{
    "code": 200,
    "data":
    {
        "id": 9900,
        "category": {
            "id": 9900,
            "name": "cats"
        },
        "name": "momo",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
            "id": 9900,
            "name": "momo the cat"
            }
        ],
        "status": "available"
    }
}
```

- Scenario 02: Verify able to add a new pet data with status ("pending")
- Result: Passed
```
// Adding data (Before)
{
    "id": 9901,
    "category": {
        "id": 9901,
        "name": "dogs"
    },
    "name": "shiro",
    "photoUrls": [
        "string"
    ],
    "tags": [
        {
        "id": 9901,
        "name": "shiro the dog"
        }
    ],
    "status": "pending"
}
```
```
// Successful in storing the data (After)
{
    "code": 200,
    "data":
    {
        "id": 9901,
        "category": {
            "id": 9901,
            "name": "dogs"
        },
        "name": "shiro",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
            "id": 9901,
            "name": "shiro the dog"
            }
        ],
        "status": "pending"
    }
}
```

- Scenario 03: Verify able to add a new pet data with status ("sold")
- Result: Passed
```
// Adding data (Before)
{
    "id": 9902,
    "category": {
        "id": 9902,
        "name": "dogs"
    },
    "name": "jasper",
    "photoUrls": [
        "string"
    ],
    "tags": [
        {
            "id": 9902,
            "name": "jasper the dog"
        }
    ],
    "status": "sold"
}
```
```
// Successful in storing the data (After)
{
    "code": 200,
    "data":
    {
        {
        "id": 9902,
        "category": {
            "id": 9902,
            "name": "dogs"
        },
        "name": "jasper",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 9902,
                "name": "jasper the dog"
            }
        ],
        "status": "sold"
    }
}
```

Endpoint: https://petstore.swagger.io/#/pet/findPetsByStatus [GET]
- https://petstore.swagger.io/v2/pet/findByStatus?status=available [GET]
- Scenario 04: Verify able to filter pet data by status  ("available")
- Parameter: status [Key] available [Value]
- Result: Passed
```
{
    "code": 200,
    "data":
    [
        {
            "id": 9900,
            "category": {
                "id": 9900,
                "name": "cats"
            },
            "name": "momo",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9900,
                    "name": "momo the cat"
                }
            ],
            "status": "available"
        },
        {
            "id": 9903,
            "category": {
                "id": 9903,
                "name": "cats"
            },
            "name": "mimi",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9903,
                    "name": "mimi the cat"
                }
            ],
            "status": "available"
        },
        {
            "id": 9904,
            "category": {
                "id": 9904,
                "name": "cats"
            },
            "name": "milo",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9904,
                    "name": "milo the cat"
                }
            ],
            "status": "available"
        }
    ]
}
```

- https://petstore.swagger.io/v2/pet/findByStatus?status=pending [GET]
- Scenario 05: Verify able to filter pet data by status  ("pending")
- Parameter: status [Key] pending [Value]
- Result: Passed
```
{
    "code": 200,
    "data":
    [
       {
            "id": 9901,
            "category": {
                "id": 9901,
                "name": "dogs"
            },
            "name": "shiro",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9901,
                    "name": "shiro the dog"
                }
            ],
            "status": "pending"
        },
        {
            "id": 9905,
            "category": {
                "id": 9905,
                "name": "cats"
            },
            "name": "milo",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9905,
                    "name": "tompok the cat"
                }
            ],
            "status": "pending"
        },
        {
            "id": 9906,
            "category": {
                "id": 9906,
                "name": "dogs"
            },
            "name": "ollie",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9906,
                    "name": "ollie the dog"
                }
            ],
            "status": "pending"
        }
    ]   
}
```

- https://petstore.swagger.io/v2/pet/findByStatus?status=sold [GET]
- Scenario 06: Verify able to filter pet data by status  ("sold")
- Parameter: status [Key] sold [Value]
- Result: Passed
```
{
    "code": 200,
    "data":
    [
        {
            "id": 9902,
            "category": {
                "id": 9902,
                "name": "dogs"
            },
            "name": "jasper",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9902,
                    "name": "jasper the dog"
                }
            ],
            "status": "sold"
        },
        {
            "id": 9907,
            "category": {
                "id": 9907,
                "name": "dogs"
            },
            "name": "charlie",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9907,
                    "name": "charlie the dog"
                }
            ],
            "status": "sold"
        },
        {
            "id": 9908,
            "category": {
                "id": 9908,
                "name": "cats"
            },
            "name": "itam",
            "photoUrls": [
            "string"
            ],
            "tags": [
                {
                    "id": 9908,
                    "name": "itam the cat"
                }
            ],
            "status": "sold"
        }
    ]   
}
```

Endpoint: https://petstore.swagger.io/#/pet/updatePet [PUT] (Failed)
- Scenario 07: Verify able to update on the existing pet data (Eg: id:9908)
- Result: Passed
```
// Updating the data (Before)
{
    "id": 9908,
    "category": {
        "id": 9908,
        "name": "cats"
    },
    "name": "itam",
    "photoUrls": [
    "string"
    ],
    "tags": [
        {
            "id": 9908,
            "name": "itam the cat"
        }
    ],
    "status": "sold"
}
```

```
// Successful updating existing data (After)
{
    "code": 200,
    "data":
    {
        "id": 9908,
        "category": {
            "id": 9908,
            "name": "cats"
        },
        "name": "putih",
        "photoUrls": [
        "string"
        ],
        "tags": [
            {
                "id": 9908,
                "name": "putih the cat"
            }
        ],
        "status": "available"
    }
}
```
- Scenario 08: Verify able to update on non-existing pet data (Eg: id:8999)
- Result: Failed, expectation on "code":404 instead of "code":200
```
// Updating the non-existing data (Before)
{
    "id": 8999,
    "category": {
    "id": 8999,
    "name": "string"
    },
    "name": "a",
    "photoUrls": [
    "string"
    ],
    "tags": [
        {
            "id": 8999,
            "name": "a"
        }
    ],
    "status": "available"
}
```
```
// Successful updating non-existing data (After). the  "code" should be 404 (Eg: "code":404)
{
    "code": 200,
    "data":
    {
        "id": 8999,
        "category": {
            "id": 8999,
            "name": "string"
        },
        "name": "a",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 8999,
                "name": "a"
            }
        ],
        "status": "available"
    }
}
```


Endpoint: https://petstore.swagger.io/#/pet/deletePet [DELETE]
- Scenario 09: Verify able to delete the existing pet data (Eg: id:9907)
- Result: Passed
```
{
  "code": 200,
  "type": "unknown",
  "message": "9907"
}
```
- Scenario 10: Verify on unable to delete the non-existing pet data  (Eg: id:9907)
- Result: Passed
```
{
  "code": 404,
  "type": "unknown",
  "message": "not found"
}
```

Endpoint: https://petstore.swagger.io/#/pet/getPetById [GET]
- https://petstore.swagger.io/v2/pet/9900
- Scenario 11: Verify able to get pet data by id (Eg: id:9900)
- Result: Passed
```
{
    "code": 200,
    "data":
    {
        "id": 9900,
        "category": {
            "id": 9900,
            "name": "cats"
        },
        "name": "momo",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 9900,
                "name": "momo the cat"
            }
        ],
        "status": "available"
    }
}
```
- Scenario 12: Verify unable to get non-existing pet data by id (Eg: id:9907)
- Result: Passed
```
{
    "code": 404,
    "data":
    {
        "code": 1,
        "type": "error",
        "message": "Pet not found"
    }
    
}
```

Endpoint: https://petstore.swagger.io/#/store/placeOrder [POST]
- Scenario 13: Verify able to add order data with existing pet data
- Result: Passed
```
// Add order data for an existing pet data (Before)
{
    "id": 9,
    "petId": 9900,
    "quantity": 2,
    "shipDate": "2022-09-30T17:01:39.307+0000",
    "status": "placed",
    "complete": false
}
```
```
// Successful add order data for an existing pet data (After)
{   
    "code": 200,
    "data":
    {
        "id": 9,
        "petId": 9900,
        "quantity": 2,
        "shipDate": "2022-09-30T17:01:39.307+0000",
        "status": "placed",
        "complete": false
    }
}
```
- Scenario 14: Verify able to add order data with non-existing pet data
- Result: Failed, expectation on "code":404 instead of "code":200
```
// Add order data for non-existing pet data (Before)
{
    "id": 9912312300,
    "petId": 9912312300,
    "quantity": 2,
    "shipDate": "2022-09-30T17:01:39.307+0000",
    "status": "placed",
    "complete": false
}
```
```
// Successful add order data for non-existing pet data (After)
{   
    "code": 200,
    "data":
    {
        "id": 9912312300,
        "petId": 9912312300,
        "quantity": 2,
        shipDate": "2022-09-30T17:01:39.307+0000",
        "status": "placed",
        "complete": false
    }
}
```

Endpoint: https://petstore.swagger.io/#/store/getOrderById [GET]
- Scenario 15: Verify able to filter order data by order id (Eg: id:9)
- Result: Passed
```
{   
    "code": 200,
    "data":
    {
        "id": 9,
        "petId": 9900,
        "quantity": 2,
        "shipDate": "2022-09-30T17:01:39.307+0000",
        "status": "placed",
        "complete": false
    }
}
```
- Scenario 16: Verify able to filter non-existing order data by order id (Eg: id:1)
- Result: Passed
```
{   
    "code": 404,
    "data":
    {
        "code": 1,
        "type": "error",
        "message": "Order not found"
    }
}
```

Endpoint: https://petstore.swagger.io/#/store/deleteOrder [DELETE]
- Scenario 17: Verify able to delete existing order data by order id (Eg: id:9)
- Result: Passed
```
{
    "code": 200,
    "type": "unknown",
    "message": "9"
}
```
- Scenario 18: Verify able to delete non-existing order data by order id (Eg: id:1)
- Result: Passed
```
{
  "code": 404,
  "type": "unknown",
  "message": "Order Not Found"
}
```

Endpoint: https://petstore.swagger.io/#/user/createUsersWithListInput [POST]
- Scenario 19: Verify able to create list of users into the array
- Result: Passed
```
// Create list of users into array (Before)
[
  {
    "id": 1,
    "username": "famir",
    "firstName": "fahmi",
    "lastName": "imran",
    "email": "fahmimran1@gmail.com",
    "password": "F12345",
    "phone": "011-1245987",
    "userStatus": 1
  },
  {
    "id": 2,
    "username": "amirul",
    "firstName": "ahmad",
    "lastName": "amirul",
    "email": "ahmadamirul1@gmail.com",
    "password": "A12345",
    "phone": "011-1256773",
    "userStatus": 0
  }

]
```
```
// Successful create list of users into array (After)
{
  "code": 200,
  "type": "unknown",
  "message": "ok"
}
```

Endpoint: https://petstore.swagger.io/#/user/getUserByName [GET]
- Scenario 20: Verify able to filter an existing user by username
- Result: Passed
```
{
    "code": 200,
    "data":
    {
        "id": 1,
        "username": "famir",
        "firstName": "fahmi",
        "lastName": "imran",
        "email": "fahmimran1@gmail.com",
        "password": "F12345",
        "phone": "011-1245987",
        "userStatus": 1
    }
}
```
- Scenario 21: Verify able to filter non-existing user by username
- Result: Passed
```
{
    "code": 404,
    "data":
    {
        "code": 1,
        "type": "error",
        "message": "User not found"
    }
}
```

Endpoint: https://petstore.swagger.io/#/user/updateUser [PUT]
- Scenario 22: Verify able to update on an existing user by user name
- Result: Passed
```
// Update on an existing user by username (Before)
{
    "id": 2,
    "username": "amirul",
    "firstName": "ahmad",
    "lastName": "amirul",
    "email": "ahmadamirul1@gmail.com",
    "password": "A12345",
    "phone": "011-1256773",
    "userStatus": 0
}
```
```
{
    // Successful update details on an existing user by username (After)
    {
    "code": 200,
    "type": "unknown",
    "message": "2"
    }
    // (updated version)
    {
        "id": 2,
        "username": "amirul",
        "firstName": "ahmad",
        "lastName": "amirul",
        "email": "amerul@gmail.com",
        "password": "M12345",
        "phone": "012-2256773",
        "userStatus": 1
    }
}
```
- Scenario 23: Verify able to update on non-existing user by user name
- Result: Failed, expectation on "code":404 instead of "code":200
```
{
    // Successful update details on non-existing user by username
    {
    "code": 200,
    "type": "unknown",
    "message": "3"
    }
    {
        "id": 3,
        "username": "amori",
        "firstName": "amori",
        "lastName": "amori",
        "email": "amori@gmail.com",
        "password": "amori2445",
        "phone": "012-12312412",
        "userStatus": 1
    }
}
```

Endpoint: https://petstore.swagger.io/#/user/deleteUser [DELETE]
- Scenario 24: Verify able to delete on an existing user by username
- Result: Passed
```
// Successful delete an existing user by using username
{
  "code": 200,
  "type": "unknown",
  "message": "amirul"
}
```
- Scenario 25: Verify able to delete on non-existing user by username
- Result: Passed
```
// Unable to delete on non-existing user by using username
{
  "code": 404,
  "type": "unknown",
  "message": "User not found"
}
```

